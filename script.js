// Реалізувати функцію, яка виконуватиме математичні операції з введеними користувачем числами.
// Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

// Технічні вимоги:
// Отримати за допомогою модального вікна браузера два числа.

// Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати.
// Сюди може бути введено +, -, *, /.

// Створити функцію, в яку передати два значення та операцію.

// Вивести у консоль результат виконання функції.

// Необов'язкове завдання підвищеної складності

// Після введення даних додати перевірку їхньої коректності.Якщо користувач не ввів числа,
// або при вводі вказав не числа, - запитати обидва числа знову(при цьому значенням за замовчуванням для кожної зі
// змінних має бути введена інформація раніше).

"use strict";

function getNumber(title) {
  // Function getting number with any title from user
  let number;

  do {
    number = prompt(`Input the ${title} number:`, number);
  } while (!+number && number !== "0"); // Checking inputted number

  return +number;
}

function getOperation() {
  // Function getting operation from user
  let operation;

  do {
    operation = prompt("Input operation (+, -, *, /):");
  } while (!"+-*/".includes(operation)); //Checking the user inputted valid operator

  return operation;
}

function passOperation(firstNumber, secondNumber, operator) {
  // Passing operation using inputted numbers and operator
  switch (operator) {
    case "+":
      return firstNumber + secondNumber;
    case "-":
      return firstNumber - secondNumber;
    case "*":
      return firstNumber * secondNumber;
    case "/":
      return firstNumber / secondNumber;
  }
}

console.log(
  passOperation(getNumber("first"), getNumber("second"), getOperation())
);
